<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>php chess board</title>
    <style>
    body{
        height:99vh;
        width:99vw;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
    }
    td{
        border: none;
    }
    table{
        border: 5px solid #333333;
        box-shadow: 4px 4px 16px black;
    }
    </style>
</head>
<body>
    

    <div>
    <h3>Chessboard <sub>by Abhishek</sub>  </h3>
    
    </div>
    <table width="400px" cellspacing="0px" cellpadding="0px" border="1px">
<?php
for($row=1;$row<=8;$row++)
{
	echo "<tr>";
	for($column=1;$column<=8;$column++)
	{
		$total=$row+$column;
		if($total%2==0)
		{
			echo "<td height=50px width=50px bgcolor=#FFFFFF></td>";
		}
		else
		{
			echo "<td height=50px width=50px bgcolor=#333333></td>";
		}
	}
	echo "</tr>";
}
?>
</table>
</body>
</html>