<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h2>Student information</h2>
  <table border="1">
    <tr bgcolor="#9acd32">
      <th>Name</th>
      <th>Enrollment Number</th>
      <th>Specialization</th>
      <th>NPTEL course</th>
      <th>SGPA</th>
      <th>CGPA</th>
    </tr>
    <xsl:for-each select="studentStore/student">
    <tr>
      <td><xsl:value-of select="name"/></td>
      <td><xsl:value-of select="enroll"/></td>
      <td><xsl:value-of select="specialization"/></td>
      <td><xsl:value-of select="nptel"/></td>
      <td><xsl:value-of select="sgpa"/></td>
      <td><xsl:value-of select="cgpa"/></td>
    </tr>
    </xsl:for-each>
  </table>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet> 